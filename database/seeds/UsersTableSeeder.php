<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {
            $role = Role::where('name', 'admin')->firstOrFail();

            User::create([
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => bcrypt('password'),
                'nick' => str_random(10),
                'access_key' => str_random(10),
                'cellphone' => str_random(10),
                'obs' => str_random(10),
                'remember_token' => str_random(60),
                'role_id'        => $role->id,
            ]);
        }
    }
}
