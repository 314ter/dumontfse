<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('airplane_id')->unsigned();
            $table->foreign('airplane_id')->references('id')->on('airplanes')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('simulator_id')->unsigned();
            $table->foreign('simulator_id')->references('id')->on('simulators')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('producer_id')->unsigned();
            $table->foreign('producer_id')->references('id')->on('producers')->onUpdate('cascade')->onDelete('cascade');
            $table->string('link');
            $table->text('obs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('designs');
    }
}
