<link rel="stylesheet" href="{{asset('css/header.css')}}">
<header>
    <div class="header">
        <nav>
            <div class="nav-wrapper container-small">
                <a href="#!" class="brand-logo"><img src="{{asset('img/new_logo_dumont.png')}}"></a>
                <a href="#" data-target="mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="/">Home</a></li>
                    <li><a href="/frota">Frota</a></li>
                    @if(Auth::user())
                        <li><a href="/texturas">Texturas</a></li>
                    @endif
                    <li><a class="dropdown-trigger" href="#!" data-target="fseNormal">FSE<i class="material-icons right">arrow_drop_down</i></a></li>
                    {{--<li><a href="/downloads">Downloads</a></li>--}}
                    @if(!$user = Auth::user())
                    <li><a class="modal-trigger" href="#modalLogin">Entrar<i class="material-icons right">airplanemode_active</i></a></li>
                    @else
                    <li><a class="dropdown-trigger" href="#!" data-target="perfilNormal">{{ $user->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                    @endif
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <ul id="fseNormal" class="dropdown-content">
            <li><a href="/fse/fbo">FBO</a></li>
        </ul>
        <ul id="fse" class="dropdown-content">
            <li><a href="/fse/fbo">FBO</a></li>
        </ul>
        <ul id="perfilNormal" class="dropdown-content">
            {{--  <li><a href="/perfil">Perfil</a></li> --}}
            @if(Voyager::can('browse_admin'))
                <li><a href="/admin">Admin</a></li>
            @endif
            <li><a href="/logout">Sair</a></li>
        </ul>
        <ul id="perfil" class="dropdown-content">
            {{-- <li><a href="/perfil">Perfil</a></li> --}}
            @if(Voyager::can('browse_admin'))
                <li><a href="/admin">Admin</a></li>
            @endif
            <li><a href="/logout">Sair</a></li>
        </ul>
        <div class="container">
            <ul class="sidenav" id="mobile">
                <li><a href="/">Home</a></li>
                <li><a href="/frota">Frota</a></li>
                <li><a class="dropdown-trigger" href="#!" data-target="fse">FSE</a></li>
                @if(Auth::user())
                    <li><a href="/texturas">Texturas</a></li>
                @endif
                {{--<li><a href="/downloads">Downloads</a></li>--}}
                @if(!$user = Auth::user())
                    <li><a class="modal-trigger" href="#modalLogin">Entrar</a></li>
                @else
                    <li><a class="dropdown-trigger" href="#!" data-target="perfil">{{ $user->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                @endif
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="carousel carousel-slider">
            @forelse($images as $image)
                <a class="carousel-item" href="#!"><img src="{{asset('/storage/'.$image->image)}}"></a>
            @empty
                <a class="carousel-item" href="#!"><h2>Não temos nenhum slider cadastrado</h2></a>
            @endforelse
        </div>
    <div class="midias">
        <nav>
            <div class="nav-wrapper container-small">
                <ul class="right">
                    <li><a href="https://www.facebook.com/dumontlogbrasil/"><img src="{{asset('img/facebook.png')}}"></a></li>
                    <li><a href="https://www.youtube.com/channel/UCduJgOly7qRVslVJq5pnTRQ"><img src="{{asset('img/youtube.png')}}"></a></li>
                    <li><a href="https://discordapp.com/invite/pgAxnf"><img src="{{asset('img/discord.png')}}"></a></li>
                </ul>
            </div>
        </nav>
    </div>

    <div id="modalLogin" class="modal modal-small">
        <div class="container">
            <div class="modal-content">
                <div class="row title">
                    <h3>Dumont LOGin :)</h3>
                </div>
                <div class="row">
                    <h6>Ainda não tenho login, desejo me <a href="/cadastro">cadastrar.</a></h6>
                </div>
                <div class="col s12 m6 l6">
                    <form class="cadastro-form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <input id="nick" type="text" class="validate" name="nick">
                                <label for="nick">Email ou Nick FSE :</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <input id="password" type="password" class="validate" name="password">
                                <label for="password">Senha:</label>
                            </div>
                        </div>
                        <div class="row right">
                            <div class="input-field col s12 m12 l12">
                                <button class="btn waves-effect waves-light orange" type="submit" name="action">Logar
                                    <i class="material-icons right">flight_takeoff</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    {{-- <a href="#!" class="modal-close waves-effect waves-green btn-flat">Logar</a> --}}
                </div>
            </div>
        </div>
    </div>
</header>

