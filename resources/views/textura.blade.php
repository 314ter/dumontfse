@extends("layouts.base")
@include("includes.header")
@section("content")
    <h2>{{ $textures[0]->airplane->description }}</h2>
    @forelse($textures as $texture)
        <div class="row">
            <div class="col s12 m12 l12">
                <p><strong>Simulador:</strong> <a href="{{ $texture->simulator->link }}">{{ $texture->simulator->name }}</a></p>
                <p><strong>Produtora:</strong> <a href="{{ $texture->producer->link }}">{{ $texture->producer->name }}</a></p>
                <p><strong>Download:</strong> <a href="{{ $texture->link }}">{{ $texture->link }}</a></p>
            </div>
        </div>
        <hr>
    @empty
        <p>Ainda não temos texturas para este avião!</p>
    @endforelse
@endsection