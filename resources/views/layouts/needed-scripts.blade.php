<script type="text/javascript" src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/materialize.min.js')}}"></script>

<script>
    $(document).ready(function(){
        $(".dropdown-trigger").dropdown();
        $('.sidenav').sidenav();
        $('.carousel').carousel({
            duration: 400,
            fullWidth: true
        });
        $('.modal').modal();
        autoplay()   
        function autoplay() {
            setTimeout(autoplay, 5000);
            $('.carousel').carousel('next');
        }

    })
</script>

@if(session()->has('message.level'))
    <script>
            M.toast({html: '<i class="material-icons right">star</i>@php echo session('message.content') @endphp', classes: 'registerToast', displayLength: 5000});
    </script>
@endif