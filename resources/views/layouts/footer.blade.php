<footer>
    <div class="container center">
        <p>Copyright © @php echo date("Y") @endphp DUMONT LOG BRASIL. Todos os direitos reservados | Made with <span class='red-text'><3</span> by <a href="mailto:piter_og@hotmail.com">Piter Ortiz</p>
    </div>
</footer>