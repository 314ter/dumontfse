@include("layouts.header")

</head>
<body>
    <div class="container">
        @yield("content")
    </div>
    <script type="text/javascript" src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/materialize.min.js')}}"></script>

</body>
@include("layouts.needed-scripts")

@include("layouts.footer")