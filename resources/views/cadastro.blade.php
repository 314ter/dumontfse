@extends("layouts.base")
@include("includes.header")
@section("content")

<style>
    .cadastro-content{
        margin-top: 50px;
    }

    .help-icon{
        margin-top: 20px;
        color: #FF712C;
    }

    .modal-content img{
        max-width: 500px;
    }

    .orange-text{
        color: #FF712C !important;
    }
    
    @media only screen and (max-width: 992px){
        .modal-content img{
            width: inherit;
        }
    }
</style>

<div class="row cadastro-content">
    <div class="col s12 m6 l6">
        <h5 class="orange-text">Passos para o cadastro:</h5>
        <p><strong>1)</strong> Curta nossa página no <a href="https://www.facebook.com/dumontlogbrasil/">Facebook</a>;</p>
        <p><strong>2)</strong> Preencha o cadastro ao lado, com todas as informações requisitadas;</p>
        <p><strong>3)</strong> Entre em nosso grupo do <a href="https://chat.whatsapp.com/138j9IcgrUN2QLeTFmiAFu">WhatsApp</a> para uma breve entrevista;</p>
        <p><strong>4)</strong> Aguarde o convite;</p>
        <p><strong>ATENÇÃO:</strong> Nosso cadastro é normalmente atualizado 2 vezes ao dia, caso tenha solicitado e ainda não tenha recebido o convite, aguarde que logo receberá. Enquanto isso poderá interagir no grupo do WhatsApp;</p>
    </div>
    <div class="col s12 m6 l6">
            @if ($errors->any())
                <div class="row">
                    <div class="col s12 m12 l12">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <form action="/register" method="POST" class="cadastro-form">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-field col s11 m11 l11">
                        <input id="name" type="text" class="validate" name="name">
                        <label for="name">Nome:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s11 m11 l11">
                        <input id="nick" type="text" class="validate" name="nick">
                        <label for="nick">Nick FSE:</label>
                    </div>
                    <div class="input-field col s1 m1 l1">
                        <a class="modal-trigger help-icon" href="#modal_nick"><i class="material-icons">live_help</i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s11 m11 l11">
                        <input id="access_key" type="text" class="validate" name="access_key">
                        <label for="access_key">Chave de Acesso:</label>
                    </div>
                    <div class="input-field col s1 m1 l1">
                        <a class="modal-trigger help-icon" href="#modal_access"><i class="material-icons">live_help</i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s11 m11 l11">
                        <input id="cellphone" type="text" class="validate" name="cellphone">
                        <label for="cellphone">Celular(whatsapp):</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s11 m11 l11">
                        <input id="email" type="text" class="validate" name="email">
                        <label for="email">E-mail:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s11 m11 l11">
                        <input id="password" type="password" class="validate" name="password">
                        <label for="password">Senha:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s11 m11 l11">
                        <textarea id="obs" class="materialize-textarea" name="obs"></textarea>
                        <label for="obs">Obs:</label>
                    </div>
                </div>
                <div class="row right">
                    <div class="input-field col s11 m11 l11">
                        <button class="btn waves-effect waves-light orange" type="submit" name="action">Cadastrar
                            <i class="material-icons right">flight_takeoff</i>
                        </button>
                    </div>
                    <div class="input-field col s1 m1 l1">
                    </div>
                </div>
            </form>
        </div>
</div>



<!-- Modal Structure -->
<div id="modal_nick" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h5>Deseja encontrar seu nick?</h5>
    <p>Entre <a href="http://server.fseconomy.net">aqui</a>, se já estiver logado, no canto superior direito estará seu nick, como mostra a imagem a abaixo: </p>
    <img src="{{ asset("img/cadastro/cadastro_nick.png") }}" alt="">
    <p>Caso ainda não estiver logado, o seu usuário é o nick que precisamos. </p>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
  </div>
</div>

<div id="modal_access" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h5>Deseja encontrar sua chave de acesso?</h5>
    <p>Após estar logado no FSEconomy, entre <a href="http://server.fseconomy.net/datafeeds.jsp">aqui</a>, caso ainda não tenha gerado uma chave de acesso(access key), clique em RESET e irá aparecer uma nova chave de acesso: </p>
    <img src="{{ asset("img/cadastro/cadastro_access.jpg") }}" alt="">
    <p>É com essa chave que conseguimos rastrear suas milhas, caso gere uma nova, é extremamente necessário informar a administração. </p>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
  </div>
</div>


<script>


</script>


@endsection