@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @forelse($notices as $n)
            @can('view_notice', $n)
                <h1>{{ $n->title }}</h1>
                <p>{{ $n->message }}</p>
                <b>{{ $n->user->name }}</b>
                <hr>
            @endcan
        @empty
            <p>Nenhuma notícia no sistema.</p>
        @endforelse
    </div>
</div>
@endsection
