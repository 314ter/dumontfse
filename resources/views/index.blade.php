@extends("layouts.base")
@include("includes.header")
@section("content")
    <link rel="stylesheet" href="{{asset('css/index.css')}}">
    <div class="row news center">
        <h4>Notícias</h4>
        <div class="col s12 m12 l12 center">
            <p class="red">-- BREVE --</p>
        </div>
    </div>

    <div class="row fse-intro">
        <div class="col s12 m6 l6">
            <div class="center title-image">
                <img src="{{asset("img/new_logo_dumont.png")}}" alt="">    
            </div>
            <p>Somos um grupo de simulação aérea virtual que utiliza os softwares Microsoft Flight Simulator X, P3D, X-Plane e que voa usando uma comunidade da web chamada FSECONOMY.</p>
            <p>Os pilotos que se juntarem a nós poderão desfrutar de boas horas de voos com os melhores aviões como o Pilatus - PC12, o Embraer Phenom 300, Sêneca e muitos outros que poderão ser observados na nossa frota completa.</p>
        </div>
        <div class="col s12 m6 l6" >
            <div class="center title-image">
                <img class="center" src="{{asset("img/fselogo.jpg")}}" alt="">
            </div>
            <p>FSEconomy é um ambiente onde os entusiastas da simulação de voo podem adicionar um novo aspecto ao seu vôo. Desde 2005, o FSEconomy permitiu que mais de 10.000 pilotos de simuladores registrados ganhassem dinheiro virtual no jogo, pilotando aeronaves da aviação geral de e para quase todos os aeroportos da Terra.</p>
            <p>Com esses ganhos virtuais, os pilotos podem comprar seus próprios aviões, entrar ou iniciar negócios virtuais com outros membros, operar FBOs e muito mais - tudo dentro do mundo livre da FSEconomy.</p>
            <p>O FSEconomy adiciona uma nova dimensão à sua experiência de simulação de voo, e isso nem conta todas as pessoas que você conhecerá ao longo do caminho.</p>
        </div>
    </div>
    <div class="row center">
        <div class="col s12 m12 l12">
            <h4>Parceiros</h4>
        </div>
    </div>
    <div class="row center">
        <div class="container">
            <div class="col s12 m6 l3 logo-parceiros">
                <img src="{{asset('img/asabrasil.png')}}" alt="">
            </div>
            <div class="col s12 m6 l3 logo-parceiros">
                <img src="{{asset('img/heliforce.png')}}" alt="">
            </div>
            <div class="col s12 m6 l3 logo-parceiros">
                <img src="{{asset('img/planeair.png')}}" alt="">
            </div>
            <div class="col s12 m6 l3 logo-parceiros">
                <img src="{{asset('img/sagataxiaereo.png')}}" alt="">
            </div>
        </div>
    </div>

@endsection

