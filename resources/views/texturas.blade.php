@extends("layouts.base")
@include("includes.header")
@section("content")
@forelse($texturas as $textura)
    <h2>{{ $textura->airplane->description }}</h2>
        <div class="row">
            <div class="col s12 m12 l12">
                <p><strong>Simulador:</strong> <a href="{{ $textura->simulator->link }}">{{ $textura->simulator->name }}</a></p>
                <p><strong>Produtora:</strong> <a href="{{ $textura->producer->link }}">{{ $textura->producer->name }}</a></p>
                <p><strong>Download:</strong> <a href="{{ $textura->link }}">{{ $textura->link }}</a></p>
            </div>
        </div>
        <hr>
    @empty
        <p>Ainda não temos texturas para este avião!</p>
    @endforelse
@endsection