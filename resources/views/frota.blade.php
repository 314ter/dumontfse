@extends("layouts.base")
@include("includes.header")
@section("content")

<style>
    .card .card-title{
        bottom: -40px !important;
        color: #FF712C !important;
        font-weight: 400;
    }

    .card .card-image {
        position: relative;
        widows: 100%;
        height: 250px;
    }
    .card .card-image img{
        max-width: 250px
        margin: 0 auto;
    }


</style>

<h1 class="title">Frota</h1>
<div class="row">
    @forelse($fleets as $fleet)
    <div class="col s12 m6 l4">
        <div class="card">
            <div class="card-image">
                <a href="{{$fleet->slug}}"><img src="{{ asset($fleet->getImage($fleet->id)) }}"></a>
                <span class="card-title">{{ $fleet->airplane->description }}</span>
            </div>
            <div class="card-content">
                <p><strong>Matrícula:</strong> <a href="{{$fleet->slug}}">{{ $fleet->registration }}</a></p>
                <p><strong>Base:</strong> <a href="http://server.fseconomy.net/airport.jsp?icao={{ $fleet->base }}">{{$fleet->base}}</a></p>
                <p><strong>Assentos:</strong> {{ $fleet->airplane->seets }}</p>
                <p><strong>Cruzeiro:</strong> {{ $fleet->airplane->cruise }}</p>
                <p><strong>Equipamentos:</strong> {{ $fleet->getEquipments($fleet->id) }}</p>
                <p><strong>Textura: {!! $fleet->getTexture($fleet, $designs) !!}</strong></p>
            </div>
            <div class="card-action">
                <a href="{{$fleet->slug}}">Descrição completa</a>
            </div>
        </div>
    </div>
    @empty
        <h2>Não temos nenhum avião cadastrado</h2>
    @endforelse
</div>
@endsection