<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Design;

class Fleet extends Model
{
    public function airplane()
    {
        return $this->belongsTo('App\Airplane');
    }

    public function getEquipments($id)
    {
        $fleet = Fleet::whereId($id)->first();
        $equip = "";
        $equip .= ($fleet->ifr == '1' ? 'IFR':'VFR');
        $equip .= ($fleet->gps == '1' ? '/GPS':'');
        $equip .= ($fleet->ap == '1' ? '/AP':'');
        return $equip;
    }
    
    public function getImage($id)
    {
        $fleet = Fleet::whereId($id)->first();
        return ( $fleet->airplane->image == NULL ? 'img/generic-airplane.png': 'storage/'.$fleet->airplane->image );

    }

    public function getTexture($fleet, $designs)
    {
        
        $count = 0;
        $texture = "";
        if(count($designs)){
            foreach($designs as $design){
                if($fleet->airplane->id == $design->airplane_id){
                    $count++;
                    $texture .= '<a href="/textura/'.str_slug($fleet->airplane->description, '-').'">['.$design->simulator->abreviation.'-'.$design->producer->name.']</a> - ';
                }
            }
        }

        if($count == 0){
            $texture = '-';
        }else{
            $texture = rtrim($texture," - ");
        }

        return $texture;
    }
}
