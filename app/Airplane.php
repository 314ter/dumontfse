<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airplane extends Model
{
    public function fleet()
    {
        return $this->belongsTo('App\Fleet');
    }
}
