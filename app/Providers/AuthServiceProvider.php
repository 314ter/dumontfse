<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Notice;
use App\User;
use App\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\Notice' => 'App\Policies\NoticePolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(Gate $gate)
    {
        //$this->registerPolicies();

        /*
        |   RETORNA TODAS AS PERMISSOES E 
        |   AS FUNÇÕES QUE CONTEM CADA PERMISSAO
        */
        $permissions = Permission::with('roles')->get();

        foreach($permissions as $permission){
            Gate::define($permission->name, function(User $user) use ($permission){
                return $user->hasPermission($permission);
            });
        }

        
        Gate::before(function(User $user, $ability){
            if($user->hasAnyRoles('Admin'))
               return true;
        });
        
    }
}
