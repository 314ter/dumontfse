<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Design extends Model
{
    public function airplane()
    {
        return $this->belongsTo('App\Airplane');
    }
    public function simulator()
    {
        return $this->belongsTo('App\Simulator');
    }
    public function producer()
    {
        return $this->belongsTo('App\Producer');
    }
}
