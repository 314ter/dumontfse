<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'nick' => 'required|string|unique:users',
            'access_key' => 'required|string',
            'cellphone' => 'required|string'
        ], [
            'name.required' => 'É necessário preencher o campo de Nome.',
            'email.required' => 'É necessário preencher o campo de Email.',
            'password.required' => 'É necessário preencher o campo de Senha.',
            'nick.required' => 'É necessário preencher o campo de Nick.',
            'access_key.required' => 'É necessário preencher o campo de Chave de Acesso.',
            'cellphone.required' => 'É necessário preencher o campo de Telefone.',

            'password.min' => 'Sua senha precisa ter pelo menos 6 caracteres',

            'nick.unique' => 'Seu nick já está sendo usado, não é você?',

            'email.unique' => 'Seu e-mail já está sendo usado, não é você?',
            'email.email' => 'Seu e-mail deve ter um formato válido. Ex: email@email.com',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'role_id' => 2,
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'nick' => $data['nick'],
            'access_key' => $data['access_key'],
            'cellphone' => $data['cellphone'],
            'obs' => $data['obs'],
            'active' => 'n'
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        if(!$this->registered($request, $user)){
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Você foi cadastrado com sucesso, aguarde um administrador entrar em contato com você!');
        }else{
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Houve um erro, caso o erro persista, contate um administrador!');
        }
        
        return $this->registered($request, $user) ?: redirect($this->redirectTo);
    }
}
