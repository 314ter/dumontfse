<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        $request->session()->flash('message.level', 'danger');
        $request->session()->flash('message.content', 'Não identificamos seu nick/email ou sua senha, tente novamente!');

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors)
            ;


        /*
            if(!$this->registered($request, $user)){
                $request->session()->flash('message.level', 'success');
                $request->session()->flash('message.content', 'Você foi cadastrado com sucesso, aguarde um administrador entrar em contato com você!');
            }else{
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'Houve um erro, caso o erro persista, contate um administrador!');
            }
            */

    }

    protected function authenticated(Request $request, $user)
    {
        $request->session()->flash('message.level', 'success');
        $nome = explode(' ', $user->name);
        $request->session()->flash('message.content', 'Seja bem-vindo comandante '.$nome[0].'!');
    }

    protected function credentials(Request $request)
    {
        $field = filter_var($request->input($this->username()), FILTER_VALIDATE_EMAIL) ? 'email' : 'nick';
        $request->merge([$field => $request->input($this->username())]);
        return $request->only($field, 'password');
    }

    public function username()
    {
        return 'nick';
    }
}
