<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImageSlider;
use App\Fleet;
use App\Design;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = ImageSlider::all();
        return view('index', compact('images'));
    }
    public function create()
    {
        $images = ImageSlider::all();
        return view('cadastro', compact('images'));
    }
    public function fleet()
    {
        $fleets = Fleet::all();
        $designs = Design::all();
        $images = ImageSlider::all();
        return view('frota', compact('images', 'fleets', 'designs'));
    }
    public function fbo()
    {
        $images = ImageSlider::all();
        return view('fbo', compact('images'));
    }

    public function textures()
    {
        $images = ImageSlider::all();
        $texturas = Design::all();
        return view('texturas', compact('images', 'texturas'));
    }

    public function texture($slug, Request $request)
    {
        $images = ImageSlider::all();
        if(Auth::user()){
            $designs = Design::all();
            foreach($designs as $key => $design){
                if(str_slug($design->airplane->description, '-') == $slug){
                    $textures[$key] = $design;
                }
            }

            if(count($textures) > 0){
                return view('textura', compact('images', 'textures'));
            }else{
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'Ainda não temos nenhuma textura para este avião, caso deseje, solicite a um administrador!');
                return redirect()->back();
            }
        }else{
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Para acessar a ára de texturas, é necessário estar logado e fazer parte da DumontLog!');
            return redirect()->back();
        }
    }
}
