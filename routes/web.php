<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

Route::get('/frota', 'HomeController@fleet')->name('fleet');
Route::get('/fse/fbo', 'HomeController@fbo')->name('fbo');

Route::get('/texturas', 'HomeController@textures')->name('textures');
Route::get('/textura/{airplane}', 'HomeController@texture')->name('texture');

Route::get('/download', function () {
    return view('download');
});

Route::get('/cadastro', 'HomeController@create')->name('create');

Route::get('/login', function () {
    return view('login');
});
Route::get('/logout', 'Auth\LoginController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
